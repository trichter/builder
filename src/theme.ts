import * as Twig from 'twig'
import * as path from 'path'
import * as fs from 'fs'
import fsTrichterLoader from './utils/twig.loader.fs-trichter'

export interface Theme {
    locale: string,
    timezone: string,
    domain: string
    getTitle: (key: string, context?: { event: any }) => string
    getTemplate: (key: string)  => Twig.Template
}

(Twig as any).extend(function(Twig) {
    fsTrichterLoader(Twig)
})


export function getTheme(name: string): Theme {
    const settings = require(`./themes/${name}/settings.js`)
    const templates: {[key: string]: Twig.Template} = {}

    for(let key of ['startsite', 'event', 'info', 'browse-month', 'groups']) {
        try {
            fs.statSync(path.join(__dirname, `themes/${name}/templates/${key}.twig`))
            templates[key] = Twig.twig({
                id: key,
                path: path.join(__dirname, `themes/${name}/templates/${key}.twig`),
                async: false
            })
        } catch(err) {
            templates[key] = Twig.twig({
                id: key,
                path: path.join(__dirname, `themes/default/templates/${key}.twig`),
                async: false
            })
        }        
    }
    return {
        locale: 'dev',
        timezone: 'Europe/Berlin',
        domain: settings.domain,
        getTitle: (key, context) => {
            const title = settings.title[key]
            if(typeof title == 'function') {
                return title(context)
            } else {
                return title
            }
        },
        getTemplate: (key) => {
            return templates[key]
        }
    }
}