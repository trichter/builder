import Group from '@trichter/db/dist/schemas/Group';
import { format } from 'date-fns';
import * as ics from 'ics'
import { Theme } from '../theme';
import { EventEntry } from './events';

function transformDate(time: Date): ics.DateArray {
    return format(time, 'yyyy-M-d-H-m').split("-").map(x => parseInt(x)) as ics.DateArray
}
function transformGroup(g: Group): ics.Person {
    if(!g) return
    return {
        name: g.name,
        // some ics parser throw an error when ORGANIZER is specified without a mail
        email: g.email || 'invalid@example.org'
    }
}

function transformEvent(e: EventEntry, domain: string): ics.EventAttributes {
    return {
        start: transformDate(e.start),
        end: e.end ? transformDate(e.end): undefined,
        title: e.title + (e.groups[0] ? ` [${e.groups.map(g => g.name).join(', ')}]` : ''),
        description: e.description,
        location: (e.locationName ? e.locationName+', ' : '')+e.address,
        url: `https://${domain}/${e.path}`,
        status: e.isCancelled ? 'CANCELLED' : undefined,
        organizer: transformGroup(e.groups[0]),
        uid: e.id+'-'+e.hash.slice(0,4)+'@trichter.cc'
    }
}

export function events2ics(events: EventEntry[], theme: Theme) {
    const res = ics.createEvents(events.map((e) => transformEvent(e, theme.domain)))
    console.log(res.error)
    if(res.error) {
        console.log(res.error)
        throw new Error('ics generation error: \n'+(res.error as any).errors.join('\n'))
    }
    return res.value
}