import * as Twig from 'twig'
import moment from 'moment'
import * as showdown from 'showdown'
import { EventEntry } from './events'
function stripTags (str: string): string {
  return str
    .replace(/<br>/g, '<br>')
    .replace(/<\/?[^>]+(>|$)/g, '')
}
function escapeGenderStar (str: string): string {
  return str.replace(/([a-zA-Z])\*([a-zA-Z])/g, '$1\\\*$2')
}

const converter = new showdown.Converter({
  simplifiedAutoLink: true,
  simpleLineBreaks: true
})
Twig.extendFilter('markdown', (value) => {
  return converter.makeHtml(escapeGenderStar(stripTags(value)))
})
Twig.extendFilter('isToday', (value) => moment(value).isSame(moment(), 'day') as any)
Twig.extendFilter('isTomorrow', (value) => moment(value).isSame(moment().add(1, 'day'), 'day') as any)
Twig.extendFilter('dayName', (value) => moment(value).format('dddd'))
Twig.extendFilter('day', (value) => moment(value).format('D'))
Twig.extendFilter('time', (value) => {
  const a = moment(value).format('HH:mm')
  return a
})
Twig.extendFilter('tidyUpAddress', (address) => {
  if (!address || !address.trim()) return ''
  return address
    .split(', ')
    .map(a => a.replace(/[0-9]{5}( |$)/, '')) // remove postcode
    .map(a => a.replace(/straße/, 'str.').trim()) // shorter street names
    .filter(a => a.trim() && !a.match(/^(Leipzig|Germany|Deutschland|Sachsen|Saxony-Anhalt)$/)) // remove obious adress parts
    .join(', ')
})

Twig.extendFilter('filterEvents', (events: EventEntry[], only: string): any => {
  if (only.toString() === 'recurring') {
    return events.filter(e => e.linkedDates.length > 0)
  } else if (only.toString() === 'regular') {
    return events.filter(e => e.linkedDates.length === 0)
  } else {
    return []
  }
})
