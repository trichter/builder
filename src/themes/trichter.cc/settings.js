module.exports = {
    locale:"de",
    timezone:"Europe/Berlin",
    domain: 'trichter.cc', // required for .ics generation
    title: {
        index: 'trichter.cc | Veranstaltungen in Leipzig',
        info: 'trichter.cc | Veranstaltungen in Leipzig',
        groups: 'Gruppen | trichter.cc | Veranstaltungen in Leipzig',
        event: ({event}) => `${event.title} | trichter.cc | Veranstaltungen in Leipzig`
    },
}
