import TrichterDB, { Group } from '@trichter/db'
import moment from 'moment'
import 'moment-timezone'
import * as path from 'path'
import { URL } from 'url'
import Logger from '@trichter/logger'
import { promises as fs } from 'fs'

import { transformEvents, groupEventsByDate, EventEntry } from './utils/events'
// import getExistingEventFiles from './utils/existingEventFiles'
import './utils/twigFilter'
import { getTheme } from './theme'
import { events2ics } from './utils/ics'

const log = Logger('trichter-builder')

const repoPath = process.argv[2]
if (!repoPath || !repoPath.trim()) {
  log.error('no repo path provided')
  process.exit(1)
}

const theme = getTheme(process.env.TRICHTER_THEME || 'trichter.cc')
moment.locale(theme.locale)
moment.tz.setDefault(theme.timezone)

const outputPath = path.join(__dirname, '../output')
const db = new TrichterDB(repoPath)


async function storeGroupImage (group: Group): Promise<void> {
  if (!group.imageFile) return
  const imagePath = `images/${group.key}/group${path.extname(group.imageFile.filename)}`
  const filePath = path.join(outputPath, imagePath)
  await fs.writeFile(filePath, group.imageFile.toBuffer())
}

async function generateEvent (data: {
  event: EventEntry
  browseLink: string
}): Promise<void> {
  const { event, browseLink } = data
  let imagePath = null
  if (event.imageFile) {
    const image = event.imageFile
    imagePath = `images/${event.groups[0].key}/${event.image}`

    const filePath = path.join(outputPath, imagePath)
    await fs.writeFile(filePath, image.toBuffer())
  }

  const upcomingLinkedDates = event.linkedDates.filter(e => {
    return moment(e.start).isAfter(moment())
  })
    .sort((a: EventEntry, b: EventEntry) => {
      return a.start.valueOf() - b.start.valueOf()
    })

  const url = event.link ? new URL(event.link) : null

  const rendered = (await theme.getTemplate('event').render({
    title: theme.getTitle('event', {event}),
    browseLink: browseLink,
    event: event,
    upcomingLinkedDates: upcomingLinkedDates,
    imagePath: imagePath,
    eventDomain: url ? url.hostname.replace(/^www\./, '') : null,
    og: {
      image: event.image,
      title: theme.getTitle('event', {event}),
      description: event.teaser,
      url: `https://${theme.domain}/${event.path}`
    }
  }))
    .replace(/\s{2,}/g, ' ') // simple minifier

  await fs.writeFile(path.join(outputPath, event.path), rendered)
}

async function generateGroupsPage (data: {
  groups: Group[]
  browseLink: string
}): Promise<void> {
  const { groups, browseLink } = data
  const rendered = (await theme.getTemplate('groups').render({
    title: theme.getTitle('groups'),
    browseLink: browseLink,
    groups: groups.sort((a, b) => a.name.localeCompare(b.name))
  }))
    .replace(/\s{2,}/g, ' ') // simple minifier

  await fs.writeFile(path.join(outputPath, 'gruppen.html'), rendered)
}

async function generateBrowsePages (data: {
  monthsWithEvents: string[]
  daysWithEvents: string[]
  eventsByDate: {[date: string]: EventEntry[]}
  browseLink: string
}): Promise<void> {
  for (const month of data.monthsWithEvents) {
    const year = month.slice(0, 4)
    const nameOfMonth = moment(month).format('MMMM')
    const rendered = theme.getTemplate('browse-month').render({
      title: `${nameOfMonth} ${year} | trichter.cc | Veranstaltungen in Leipzig`,
      domain: theme.domain,
      monthKey: month,
      months: data.monthsWithEvents.map(m => ({
        key: m,
        name: moment(m).format('MMMM'),
        year: m.slice(0, 4),
        link: `/${m.slice(0, 4)}/${moment(m).format('MMMM').toLowerCase()}.html`
      })),
      days: data.daysWithEvents.filter(d => d.indexOf(month) === 0),
      eventsByDate: data.eventsByDate,
      browseLink: data.browseLink
    })
    await fs.writeFile(path.join(outputPath, year, `${nameOfMonth.toLowerCase()}.html`), rendered)
  }
}

(async function main () {
  try {
    const branch = await db.checkoutBranch('master')
    await branch.loadData()
    log.debug(`loaded ${branch.events.length} events in ${branch.groups.length} groups`)

    try {
      log.debug('ceate events directory')
      await fs.mkdir(path.join(outputPath, 'events'))
    } catch (err) {}

    // log.debug('get existing event files')
    // const existingEventFiles = await getExistingEventFiles(outputPath)

    log.debug('group and transform events')
    const events = transformEvents(branch.events)
    const futureEvents = events
        .filter(e => e.end ? moment(e.end).isAfter(moment()) : moment(e.start).add(2, 'hours').isAfter(moment()))
        .filter(e => moment(e.start).add(3, 'days').isAfter(moment()))
    const futureEventsByDate = groupEventsByDate(futureEvents)

    const eventsByDate = groupEventsByDate(
      events
    )
    const daysWithEvents = Object.keys(eventsByDate).sort()
      .filter(day => {
        // ignore everything before 6 months ago
        if (moment(day).add(6, 'months').isBefore(moment())) return false

        // ignore everthing more than 6 months in the future
        if (moment(day).subtract(6, 'months').isAfter(moment())) return false
        return true
      })
    const monthsWithEvents = daysWithEvents.map(d => d.substr(0, 7)).filter((v, i, self) => self.indexOf(v) === i).sort()
    const yearsWithEvents = Object.keys(eventsByDate).sort().map(d => d.substr(0, 4)).filter((v, i, self) => self.indexOf(v) === i).sort()

    const browseLink = `/${moment().year()}/${moment().format('MMMM').toLowerCase()}.html`

    log.info('create folders...')
    for (const year of yearsWithEvents) {
      try {
        await fs.mkdir(path.join(outputPath, year))
      } catch (err) {}
    }
    for (const group of branch.groups) {
      try {
        await fs.mkdir(path.join(outputPath, 'images', group.key), { recursive: true })
      } catch (err) {
        console.log(err)
      }
    }

    log.info('store group images...')
    for (const group of branch.groups) {
      await storeGroupImage(group)
    }

    log.info('generate group page...')
    await generateGroupsPage({
      groups: branch.groups,
      browseLink
    })

    log.debug('generate browse pages...')
    await generateBrowsePages({
      monthsWithEvents,
      daysWithEvents,
      eventsByDate,
      browseLink
    })

    log.info('generate event pages...')
    for (const event of events) {
      // if(!event.path.match(/cq0u|aufstand/)) continue
      log.debug(`generate ${event.path}`)
      await generateEvent({
        event,
        browseLink
      })
    }

    log.info('generate index...')
    const rendered = (await theme.getTemplate('startsite').render({
      title: theme.getTitle('index'),
      domain: theme.domain,
      days: Object.keys(futureEventsByDate).sort().slice(0, 10),
      eventsByDate: futureEventsByDate,
      browseLink
    }))
      .replace(/\s{2,}/g, ' ') // simple minifier
    await fs.writeFile(path.join(outputPath, 'index.html'), rendered, 'utf-8')

    log.info('generate .ics index')
    const ics = events2ics(futureEvents, theme)
    await fs.writeFile(path.join(outputPath, 'events.ics'), ics)


    log.info('generate info...')
    const rendered2 = theme.getTemplate('info').render({
      title: theme.getTitle('info'),
      domain: theme.domain,
      days: Object.keys(futureEventsByDate).sort().slice(0, 10),
      eventsByDate: futureEventsByDate,
      browseLink
    })
    await fs.writeFile(path.join(outputPath, 'info.html'), rendered2)

    log.info('done')
    process.exit(0)
  } catch (err) {
    log.error(err)
  }
}())
