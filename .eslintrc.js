module.exports = {
    "extends": "standard-with-typescript",
    "parserOptions": {
        "project": "./tsconfig.json"
    },
    rules: {
      '@typescript-eslint/no-this-alias': 'off',
      '@typescript-eslint/strict-boolean-expressions': 'off',
      '@typescript-eslint/no-misused-promises': 'off',
      '@typescript-eslint/no-empty-interface': 'off',
      'no-control-regex': 'off',
      '@typescript-eslint/require-array-sort-compare': 'off',
      'no-useless-escape': 'off'
    }
  }
  
  